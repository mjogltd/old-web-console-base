A Docker container built as a base image capable of serving the client's web interface scripts.

This is *NOT* the place to put any web scripts, it is entirely focused on providing Apache and PHP with required modules. Put your web scripts in a derived container.
