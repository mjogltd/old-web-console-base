FROM php:5.6-apache
MAINTAINER james.green@mjog.com
RUN apt-get update && apt-get install -y \
    libmcrypt-dev \
    libssl-dev \
    libxml2-dev \
    libzip-dev && \
    docker-php-ext-install mcrypt pdo pdo_mysql mbstring soap zip
